Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng mười một và mười hai 2018)
Slug: new-developers-2018-12
Date: 2019-01-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Abhijith PA (abhijith)
  * Philippe Thierry (philou)
  * Kai-Chung Yan (seamlik)
  * Simon Qhuigley (tsimonq2)
  * Daniele Tricoli (eriol)
  * Molly de Blanc (mollydb)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Nicolas Mora
  * Wolfgang Silbermayr
  * Marcos Fouces
  * kpcyrd
  * Scott Martin Leggett

Xin chúc mừng!

