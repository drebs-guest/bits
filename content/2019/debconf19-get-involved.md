Title: Get involved in DebConf19!
Date: 2019-01-11 12:00
Tags: debconf, debconf19
Slug: debconf19-get-involved
Author: Laura Arjona Reina
Status: draft

DebConf19 will take place in Curitiba, Brazil from July XX to August YY, 2019.
It will be preceded by DebCamp, July ZZ to July TT, and Debian Day, July NN.

We invite everyone to join us in organizing DebConf19.
There are different areas where your help could be very valuable,
and we are always looking forward to your ideas.

The DebConf content team is open to suggestions for invited speakers.
If you'd like to propose somebody who is not a regular DebConf attendee
follow the details in the [call for speaker proposals](FIXME) blog post.

We are also beginning to contact potential sponsors from all around the globe.
If you know any organization that could be interested,
please consider handing them
the [sponsorship brochure](https://media.debconf.org/dc19/fundraising/debconf19_sponsorship_brochure_en.pdf)
or [contact the fundraising team](mailto:sponsors@debconf.org) with any leads.

The DebConf team is holding IRC meetings regurlarly.
Have a look at the DebConf19 [website](https://debconf19.debconf.org) and [wiki page](FIXME),
and engage in the IRC channels and the mailing list.

Let’s work together, as every year, on making the best DebConf ever!
