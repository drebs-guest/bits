Title: Debian turns 24!
Date: 2017-08-16 17:50
Tags: debian, birthday
Slug: debian-turns-24
Author: Laura Arjona Reina
Status: published

Today is Debian's 24th anniversary. If you are close to any of the cities
[celebrating Debian Day 2017](https://wiki.debian.org/DebianDay/2017), you're
very welcome to join the party!

If not, there's still time for you to organize a little celebration or
contribution to Debian. For example, spread the word about Debian Day
with this nice piece of artwork
created by Debian Developer Daniel Lenharo de Souza and Valessio Brito,
taking inspiration from the desktop themes
[Lines](https://wiki.debian.org/DebianArt/Themes/Lines)
and [softWaves](https://wiki.debian.org/DebianArt/Themes/softWaves) by Juliette Belin:

![Debian 24](|filename|/images/debian24.png)


If you also like graphics design, or design in general, have a look at
[https://wiki.debian.org/Design](https://wiki.debian.org/Design) and join
the team! Or you can visit the general list of [Debian Teams](https://wiki.debian.org/Teams)
for many other opportunities to participate in Debian development.

Thanks to everybody who has contributed to develop our beloved operating system
in these 24 years, and happy birthday Debian!
